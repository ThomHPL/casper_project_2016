#!/usr/bin/env python
# license removed for brevity
import rospy
import sys
import math
from time import sleep
from datetime import datetime
from casper_demo.msg import Sensing
from Libraries.RACOM.RACOM_TP import RACOM_TP

RacomTP = None
sensing_msg = None
pub = None
rate = None
timer = None

def initSensors():
    rospy.loginfo("Sensing Interface Initializated!")

def initTopics():
    global pub
    global rate
    pub = rospy.Publisher('Sensing_Results', Sensing, queue_size=10)
    rospy.init_node('Casper_Sensing', anonymous=True)
    rate = rospy.Rate(50) # 50hz

def Casper_Sensing():
    global pub

    while not rospy.is_shutdown():
        rospy.loginfo(sensing_msg)
        pub.publish(sensing_msg)
        rate.sleep()

if __name__ == '__main__':
    global sensing_msg
    global RacomTP
    global ports
    try:
        initTopics()
        sensing_msg = Sensing()
        RacomTP = RACOM_TP("/dev/ttyACM0")
        Casper_Sensing()
        
    except rospy.ROSInterruptException:
        pass
