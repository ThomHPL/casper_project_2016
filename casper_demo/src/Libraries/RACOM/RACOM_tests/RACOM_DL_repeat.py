import sys
from random import randint
from datetime import datetime
from time import sleep
import signal

from RACOM_DL import RACOM_DL

def print_results():
	# Compute results
	t1 = datetime.now()
	time = (t1-t0).total_seconds()
	Thru = (100.0*(float(ok)/float(i+1)))
	speed = (i+1)*payload_size/time
	# Display Results
	sys.stdout.write("\r\n")
	sys.stdout.write("Robustness test results: \r\n")
	sys.stdout.write("Interface: \t")
	print iface
	sys.stdout.write("Payload size: ")
	sys.stdout.write("\t%d\r\n" % payload_size)
	sys.stdout.write("N: ")
	sys.stdout.write("\t\t%d\r\n" % (i+1))
	sys.stdout.write("Time: ")
	sys.stdout.write("\t\t%f\ts\r\n" % time)
	sys.stdout.write("Throughput: ")
	sys.stdout.write("\t%f\t%%\r\n" % Thru)
	sys.stdout.write("Speed: ")
	sys.stdout.write("\t\t%d\t\tBps\r\n" % speed)
	
def stop_handler(signal, frame):
	sys.stdout.write("\r\nInterrupted!!\r\n")
	print_results()
	sys.exit(0)

signal.signal(signal.SIGINT, stop_handler)

def test(s):
	data = [0]
	data[1:] = range(s)
	RacomDL.send(data)
	_t0 = datetime.now()
	while True :
		code=RacomDL.available()
		if code is -1 or (datetime.now()-_t0).total_seconds() > RacomDL._TIMEOUT:
			return 0
		elif code is 1:
			break
	reply = RacomDL.read()
	if reply == data:
		return 1
	else:
		return 0

payload_size = 255
N = 10
ok = 0
ko = 0

# Display user interface
print "RACOM DATA LINK LAYER TEST 01"
iface = raw_input("Interface to test: ") or "/dev/ttyUSB0"
RacomDL = RACOM_DL(iface)
payload_size = int(raw_input("Enter payload size: ") or "8")
N = int(raw_input("Enter number of tests:") or "10")
sys.stdout.write("Testing:\r\n")

# Wait for the arduino to be ready
sleep(0.5)

# Perfom tests
t0 = datetime.now()
for i in range(N):
	tmp = test(payload_size)
	if tmp != 1:
		ko+=1
	else:
		ok+=1
	sys.stdout.write("\t %d %% complete         \r" % int((i+1.0)/(N)*100))
	sys.stdout.flush()
		
print_results()
