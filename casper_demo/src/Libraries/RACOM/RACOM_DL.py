#!/usr/bin/env python

import logging
import RACOM_PHY
from datetime import datetime
from time import sleep


class RACOM_DL(object):
    # Com properties
    _TIMEOUT = 0.5
    
    # Frame properties
    _STX = 0x02  # start

    # SM variables
    _t0 = 0
    _state = 0
    _packet = []
    _pSize = 0
    _cnt = 0
    _available = 0

    MAX_PDATA_SIZE = 0
    
    def __init__(self,interfaceName):
        self.RacomPHY = RACOM_PHY.RacomPHY(interfaceName)
        self.MAX_PDATA_SIZE = self.RacomPHY._bufSize-4
        self._TIMEOUT = self.RacomPHY._TIMEOUT

    def reset(self):
        self._available=0
        self._state = 0
        self.RacomPHY.flushRx()
        sleep(0.5)

    def formatPacket(self, packet):
        frame = []
        frame.append(self._STX)
        frame.append((len(packet)<<2) + (packet[0] & 0b11))

        for i in range(1, len(packet), 1):
            if type(packet[i]) is int:
                frame.append(packet[i])
            else:
                frame.append(ord(packet[i]))
        return frame

    def send(self,packet):
        frame = self.formatPacket(packet)
        self.RacomPHY.write(frame)

    def read(self):
        if self._available == 1:
            self._available = 0
            return self._packet
        else:
            return 0

    def pSize(self):
        return self._pSize
        
    def available(self):
        if(self.readSM()==-1):
            return -1
        return self._available
    
    def readSM(self):

        _WAITING_STATE = 0
        _START_STATE = 1
        _WAIT_DATA_STATE = 2

        if self._state is _WAITING_STATE:
            byte = None
            while ((self.RacomPHY.available() > 0)and(byte!=self._STX)):
                byte = self.RacomPHY.read()
                if len(byte) == 0:
                    return 0
                else:
                    byte=byte[0]
                    
            if byte is self._STX :
                self._cnt = 0
                self._state = _START_STATE
                self._t0 = datetime.now()
                self._available = 0

        elif self._state is _START_STATE:
            code = self.RacomPHY.available()
            if code > 0:
                self._packet = []
                self._pSize = self.RacomPHY.read()[0]
                self._pFlags = self._pSize & 0b11
                self._pSize = self._pSize >> 2
                self._packet.append(self._pFlags)
                self._cnt += 1
                self._state = _WAIT_DATA_STATE
            elif code < 0:
                self._packet = []
                try:
                    self._pSize = self.RacomPHY.read()[0]
                except:
                    return 0
                self._pFlags = self._pSize & 0b11
                self._pSize = self._pSize >> 2
                self._packet.append(self._pFlags)
                self._cnt += 1
                self._state = _WAIT_DATA_STATE
            elif (datetime.now()-self._t0).total_seconds() >= self._TIMEOUT :
                self._state = _WAITING_STATE
                return -1   
                

        elif self._state is _WAIT_DATA_STATE:
            while self.RacomPHY.available() > 0 and self._cnt < self._pSize:
                self._packet[len(self._packet):]=self.RacomPHY.read()
                self._cnt+=1
            if self.RacomPHY.available() == -1 and self._cnt < self._pSize:
                self._packet[len(self._packet):]=self.RacomPHY.readBytes(self._pSize-1)
                self._cnt+=self._pSize-1
            if (datetime.now()-self._t0).total_seconds() >= self._TIMEOUT :
                self._state = _WAITING_STATE
                return -1
            elif self._cnt == self._pSize :
                self._available = 1
                self._state = _WAITING_STATE
                return 1
        
        return 0
