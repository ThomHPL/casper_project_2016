#!/usr/bin/env python

import serial
import smbus

from time import sleep

class UART(object):
    
    _baudrate=115200
    _TIMEOUT=0.5
    _bufSize=64
    def __init__(self, name):
        self.connection = None
        self._name = name

        self.open()
        self.connection.flushInput()
        self.connection.flushOutput()

    def open(self):
        """
        Open the serial port
        """
        try:
            if self.connection == None or self.connection.isOpen() is False:
                self.connection = serial.Serial(self._name,  self._baudrate, 8, serial.PARITY_NONE, serial.STOPBITS_ONE, xonxoff=0, rtscts=0, timeout=self._TIMEOUT)
        except serial.SerialException:
            pass

    def close(self):
        """
        Close the serial port
        """
        try:
            if self.connection.isOpen() is True:
                self.connection.close()
        except serial.SerialException:
            pass

    def write(self, data):
        for i in range(0, len(data), 1):
            data[i] = data[i] & 0xFF
        data = bytearray(data)

        try:
            self.connection.write(data)
        except serial.SerialException:
            pass

    def read(self):
        data = 0
        try:
            data = self.readBytes(1)
        except:
            pass
        return data

    def readBytes(self,len):
        data = []
        if len == 0:
            return data
        try:
            data = [ord(x) for x in self.connection.read(len)]
        except:
            pass
        return data
    
    def available(self):
        data = 0
        try:
            data = self.connection.inWaiting()
        except:
            pass
        return data
    
    def flushRx(self):
        while self.available() != 0:
            self.read()

class I2C(object):
	_slaveAdress = 8
	_bufSize = 12
	_TIMEOUT = 0.5
	def __init__(self, name):
		self.bus_number = 1
		if "/i2c/" in name:
			self.slaveAdress =int(name.split("/i2c/",1)[1])
		elif type(name) is int:
			self.slaveAdress = name
		else:
			return -1
		self.open()
		return 0
	
	def open(self):
		try:
			self.bus = smbus.SMBus(1)
		except:
			pass
	
	def close(self):
		smbus.SMBus(1).close()
			
	def write(self, data):
		if type(data) == int:
			data = [data]
		try:
			self.bus.write_i2c_block_data(self.slaveAdress,data[0],data[1:])
		except:
			return 0
	
	def read(self):
		data=[]
		try:
			data=[self.bus.read_byte_data(self.slaveAdress,1)]
		except:
			return []
		return data
		
	def readBytes(self,len):
		data = []
		if len == 0:
			return []
		try:
			data = self.bus.read_i2c_block_data(self.slaveAdress,len)
		except:
			return []
		if type(data) is int:
			return [data]
		else:
			return data[:len]
		
	def flushRx(self):
		self.readBytes(32)
		return

	def available(self):
		return -1

class RacomPHY(object):
	_interface = None
	_bufSize = 12
	_TIMEOUT = 0.5
	def __init__(self, interfaceName):
		if interfaceName == "UART":
			self._interface = UART("/dev/ttyAMA0")
		elif "/dev/tty" in interfaceName:
			self._interface = UART(interfaceName)
		elif interfaceName == "I2C":
			self._interface = I2C(8)
		elif "/i2c/" in interfaceName:
			self._interface = I2C(interfaceName)
		self._bufSize=self._interface._bufSize
		self._TIMEOUT=self._interface._TIMEOUT

	def open(self):
		return self._interface.open()
	
	def close(self):
		return self._interface.close()
			
	def write(self, data):
		return self._interface.write(data)
	
	def read(self):
		return self._interface.read()
		
	def readBytes(self,len):
		return self._interface.readBytes(len)
		
	def flushRx(self):
		return self._interface.flushRx()

	def available(self):
		return self._interface.available()
