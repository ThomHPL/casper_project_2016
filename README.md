# Casper 2016 Installation Procedure

## Installing Raspbian Pixel without noobs:
Download Raspbian at https://www.raspberrypi.org/downloads/raspbian/ and follow https://www.raspberrypi.org/documentation/installation/installing-images/README.md

Tested with Raspbian Jessie

  * Version:April 2017
  * Release date:2017-04-10
  * Kernel version:4.4

If you are using the Raspberry Pi with a monitor, keyboard and a mouse, you can skip the next section and go to the Casper installation.

## Enabling ssh
Put an empty file called `ssh` in the /boot/ directory of the SD card
## SSH to the Raspberry Pi
Share your wifi access on your adapter settings and connect the Raspberry Pi by an ethernet cable to your computer
To check if the raspberry pi is accessible, try:
```sh
ping raspberrypi.mshome.net # for windows
ping raspberrypi.local 		# for linux
```
#### For linux:
```sh
ssh pi@raspberrypi.local
```
#### For Windows
Use Putty to connect to pi@raspberrypi.mshome.net. (http://www.putty.org/)

Login : pi
Password : raspberry

From there, you can continue to share your wifi connection on your computer or configure the wifi of the Raspberry Pi (depends ont the wifi dongle used, the SSID of your network and the password).

## Casper installation

The following command lines are updating the apt packages, changing the default password to casper2016 and cloning the casper project git repository. You should change the command to match any password you like!
```sh
sudo apt-get update
(echo raspberry & echo casper2016 & echo casper2016) | passwd
git clone https://bitbucket.org/ThomHPL/casper_project_2016.git ~/CASPER
cd ~/CASPER
```

Next step is to launch the configuration script. It will change the hostname of the Rasperry Pi to casper and configure the swap to 1024MB to handle the ROS compilation.

```sh
cd ~/CASPER
chmod +x ~/CASPER/config.sh
~/CASPER/config.sh
```

## The password of the Raspbery Pi is now casper2016 or the password you choose!

All you have to do left is to reconnect to the raspberry Pi with the new hostname (casper.local or casper.mshome.net) and password, and launch the shell script that will enable the use of the serial interface, the use of the I2C interface and install ROS Indigo.

```sh
cd ~/CASPER
chmod +x install.sh
./install.sh
```

This will take a lot of time (possibly hours), so use that time to enjoy a nice and warm coffee, to take a walk, or to do wathever you like.

Once this process is finished, the Raspberry Pi will reboot. Keep in mind that to connect to it again, the hostname is now casper.local or casper.mshome.net, and the password is either casper2016 or the password you choose earlier.
Now you can creat the catkin workspace and include th casper_demo package to test the system by typing these commands:

```sh
mkdir -p ~/catkin_ws/src/casper_demo
sudo cp -R ~/CASPER/casper_demo/* ~/catkin_ws/src/casper_demo/
cd ~/catkin_ws
catkin_make
```
To make this demo run, ensure that an Arduino Uno is connected to a USB port of the Raspberry Pi. open a new command prompt and type:

```sh
roscore
```
In your first command prompt, type:

```sh
source devel/setup.bash
rosrun casper_demo Casper_Sensing.py
```

If everything is properly installed, it should show the values of an accelerometer with every values to zero.

Now you can create your own ROS system or use the files from ~/CASPER/ROS_Casper to continue the work on the Casper platform.
