# disable console on ttyAMA
sudo sed -i 's/console=serial0,115200 / /g' /boot/cmdline.txt
sudo cp /boot/config.txt /boot/configOLD.txt
sudo bash -c "'test' >> /boot/config.txt"
sudo systemctl stop serial-getty@ttyAMA0.service
sudo systemctl disable serial-getty@ttyAMA0.service

# install I2C
sudo apt-get install python-smbus i2c-tools -y

# install ROS
sudo update-locale LANG=C LANGUAGE=C LC_ALL=C LC_MESSAGES=POSIX
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu trusty main" > /etc/apt/sources.list.d/ros-latest.list'
sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116
wget https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -O - | sudo apt-key add -
sudo apt-get update

sudo apt-get install python-pip python-setuptools python-yaml python-distribute python-docutils python-dateutil python-six -y
sudo pip install rosdep rosinstall_generator wstool rosinstall

sudo rosdep init
rosdep update
mkdir ~/ros_catkin_ws
cd ~/ros_catkin_ws
#rosinstall_generator desktop --rosdistro indigo --deps --wet-only --exclude roslisp collada_parser collada_urdf --tar > indigo-desktop-wet.rosinstall
#wstool init src indigo-desktop-wet.rosinstall
rosinstall_generator ros_comm --rosdistro indigo --deps --wet-only --exclude roslisp --tar > indigo-ros_comm-wet.rosinstall
wstool init src indigo-ros_comm-wet.rosinstall
cd ~/ros_catkin_ws
rosdep install --from-paths src --ignore-src --rosdistro indigo -y -r --os=debian:jessie

sudo ./src/catkin/bin/catkin_make_isolated --install -DCMAKE_BUILD_TYPE=Release --install-space /opt/ros/indigo -j2
echo "source /opt/ros/indigo/setup.bash" >> ~/.bashrc
sudo reboot